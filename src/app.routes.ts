import {Routes} from '@angular/router';

import {
    HomePageComponent, BandPageComponent, AlbumPageComponent,
} from './pages';


export const appRoutes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: HomePageComponent
    },
    {
        path: 'band/:bandId',
        component: BandPageComponent,
        children: [{
            path: 'album/:albumId',
            component: AlbumPageComponent,
            // children: [{
            //     path: 'track/:trackId',
            //     component: CrisisListComponent,
            // }],
        }],
    },
    {path: '**', redirectTo: '/'},
];
